# Vue 3 + TypeScript + Vite

## Create project

```
npm create vite@latest
```

## Install i18n

```
npm i @intlify/unplugin-vue-i18n
```

```
npm i vue-i18n
```

```
npm i -D @types/node
```

## Configuration
vite.config.ts
```
import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";

// IMPORTS
import VueI18nPlugin from "@intlify/unplugin-vue-i18n/vite";
import path from "path";

export default defineConfig({
  plugins: [
    ...
    // ADD THIS:
    VueI18nPlugin({
      include: [path.resolve(__dirname, "../locales/**")],
    }), 
  ],
});
```

main.ts
```
import { createApp } from 'vue'
import './style.css'
import App from './App.vue'

// IMPORTS
import { createI18n } from 'vue-i18n';
// ADD LANGS
import en from "../locales/en.json";

// CREATE I18N
const i18n = createI18n({
  locale: "en",
  messages: {
    en
  },
});

const app = createApp(App);

// USE I18N
app.use(i18n);

app.mount('#app');
```

## Use i18n con components
```
<script setup lang="ts">
import { useI18n } from "vue-i18n";

// DEFINE t
const { t } = useI18n({
  inheritLocale: true,
});

</script>

<template>
   // USE IT t('<Json Path>')
  <h1>{{ t('lang') }}</h1>
</template>
```